#!/usr/bin/env php
<?php

use FlyingAnvil\ProtonDbStatistics\Command\FetchOverviewCommand;
use FlyingAnvil\ProtonDbStatistics\Command\FetchRatingCommand;
use FlyingAnvil\ProtonDbStatistics\ContainerFactory;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\CommandLoader\ContainerCommandLoader;

require_once __DIR__ . '/../vendor/autoload.php';

(static function () {
    $root = dirname(__DIR__);
    define('PUBLIC_ROOT', $root . '/public');
    chdir($root);

    $container = ContainerFactory::create();

    $commandLoader = new ContainerCommandLoader($container, [
        FetchRatingCommand::COMMAND_NAME   => FetchRatingCommand::class,
        FetchOverviewCommand::COMMAND_NAME => FetchOverviewCommand::class,
    ]);

    $app = new Application();
    $app->setCommandLoader($commandLoader);
    $app->setCatchExceptions(true);

    $app->run();
})();


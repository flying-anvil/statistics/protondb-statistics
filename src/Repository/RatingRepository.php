<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Repository;

use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\ProtonDbStatistics\DataObject\Rating;
use PDO;

class RatingRepository
{
    public function __construct(
        private PDO $pdo,
    ) {}

    public function storeRating(Rating ...$ratings): void
    {
        $sql = <<< SQL
            INSERT INTO `ratings` (date_import, category, `range`, count_native, count_borked, count_bronze, count_silber, count_gold, count_platinum)
            VALUES (:dateImport, :category, :range, :countNative, :countBorked, :countBronze, :countSilber, :countGold, :countPlatinum)
        SQL;

        $statement = $this->pdo->prepare($sql);
        $this->pdo->beginTransaction();

        foreach ($ratings as $rating) {
            $statement->execute([
                'dateImport'    => $rating->getDateImport()->format(UtcDate::FORMAT_HUMAN),
                'category'      => $rating->getCategory()->toDatabaseString(),
                'range'         => $rating->getRange()->toDatabaseString(),
                'countNative'   => $rating->getCountNative(),
                'countBorked'   => $rating->getCountBorked(),
                'countBronze'   => $rating->getCountBronze(),
                'countSilber'   => $rating->getCountSilber(),
                'countGold'     => $rating->getCountGold(),
                'countPlatinum' => $rating->getCountPlatinum(),
            ]);
        }

        $this->pdo->commit();
    }
}

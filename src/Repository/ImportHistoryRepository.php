<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Repository;

use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\ProtonDbStatistics\DataObject\ImportHistoryEntry;
use PDO;

class ImportHistoryRepository
{
    public function __construct(
        private PDO $pdo,
    ) {}

    public function storeImportHistoryEntry(ImportHistoryEntry $historyEntry): void
    {
        $sql = <<< SQL
            INSERT INTO `import_history` (date_import, import_type)
            VALUES (:dateImport, :importType)
        SQL;

        $statement = $this->pdo->prepare($sql);
        $statement->execute([
            'dateImport' => $historyEntry->getImportDate()->format(UtcDate::FORMAT_HUMAN),
            'importType' => $historyEntry->getImportType()->toDatabaseString(),
        ]);
    }
}

<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Repository;

use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\ProtonDbStatistics\DataObject\Overview;
use PDO;

class OverviewRepository
{
    public function __construct(
        private PDO $pdo,
    ) {}

    public function storeOverview(Overview $overview): void
    {
        $sql = <<< SQL
            INSERT INTO `overview` (date_import, timestamp, count_reports, count_unique_games, count_unique_secondmost_not_topmost_rated_games, count_unique_secondmost_top_rated_games, count_unique_top_rated_games, count_works)
            VALUES (:dateImport, :timestamp, :countReports, :countUniqueGames, :countUniqueSecondmostNotTopmostRatedGames, :countUniqueSecondmostTopRatedGames, :countUniqueTopRatedGames, :countWorks)
        SQL;

        $statement = $this->pdo->prepare($sql);
        $this->pdo->beginTransaction();

        $statement->execute([
            'dateImport'                                => $overview->getDateImport()->format(UtcDate::FORMAT_HUMAN),
            'timestamp'                                 => $overview->getTimestamp(),
            'countReports'                              => $overview->getCountReports(),
            'countUniqueGames'                          => $overview->getCountUniqueGames(),
            'countUniqueSecondmostNotTopmostRatedGames' => $overview->getCountUniqueSecondmostNotTopmostRatedGames(),
            'countUniqueSecondmostTopRatedGames'        => $overview->getCountUniqueSecondmostTopRatedGames(),
            'countUniqueTopRatedGames'                  => $overview->getCountUniqueTopRatedGames(),
            'countWorks'                                => $overview->getCountWorks(),
        ]);

        $this->pdo->commit();
    }
}

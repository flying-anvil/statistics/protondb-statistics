<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Client\ApiClient\DataObject;

use FlyingAnvil\Libfa\DataObject\DataObject;
use FlyingAnvil\ProtonDbStatistics\Client\ApiClient\DataObject\Nested\Breakdown;

class SummaryResponse implements DataObject
{
    private function __construct(
        private Breakdown $breakdown,
    ) {}

    public static function create(Breakdown $breakdown): self
    {
        return new self($breakdown);
    }

    public static function fromRaw(array $rawData): self
    {
        return new self(
            Breakdown::fromRaw($rawData['breakdown'])
        );
    }

    public function getBreakdown(): Breakdown
    {
        return $this->breakdown;
    }

    public function jsonSerialize(): array
    {
        return [
            'breakdown' => $this->breakdown,
        ];
    }
}

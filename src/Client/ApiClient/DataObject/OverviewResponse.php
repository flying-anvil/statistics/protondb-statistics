<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Client\ApiClient\DataObject;

use FlyingAnvil\Libfa\DataObject\DataObject;

class OverviewResponse implements DataObject
{
    private function __construct(
        private int $timestamp,
        private int $reports,
        private int $uniqueGames,
        private int $secondmostNotTopmostRatedGames,
        private int $secondmostTopRatedGames,
        private int $topRatedGames,
        private int $works,
    ) {}

    public static function create(
        int $timestamp,
        int $reports,
        int $uniqueGames,
        int $secondmostNotTopmostRatedGames,
        int $secondmostTopRatedGames,
        int $topRatedGames,
        int $works,
    ): self {
        return new self(
            $timestamp,
            $reports,
            $uniqueGames,
            $secondmostNotTopmostRatedGames,
            $secondmostTopRatedGames,
            $topRatedGames,
            $works,
        );
    }

    public static function fromRaw(array $rawData): self
    {
        return new self(
            $rawData['timestamp'],
            $rawData['reports'],
            $rawData['uniqueGames'],
            $rawData['uniqueSecondmostNotTopmostRatedGames'],
            $rawData['uniqueSecondmostTopRatedGames'],
            $rawData['uniqueTopRatedGames'],
            $rawData['works'],
        );
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    public function getReports(): int
    {
        return $this->reports;
    }

    public function getUniqueGames(): int
    {
        return $this->uniqueGames;
    }

    public function getSecondmostNotTopmostRatedGames(): int
    {
        return $this->secondmostNotTopmostRatedGames;
    }

    public function getSecondmostTopRatedGames(): int
    {
        return $this->secondmostTopRatedGames;
    }

    public function getTopRatedGames(): int
    {
        return $this->topRatedGames;
    }

    public function getWorks(): int
    {
        return $this->works;
    }

    public function jsonSerialize(): array
    {
        return [
            'timestamp'                      => $this->timestamp,
            'reports'                        => $this->reports,
            'games'                          => $this->uniqueGames,
            'secondmostNotTopmostRatedGames' => $this->secondmostNotTopmostRatedGames,
            'secondmostTopRatedGames'        => $this->secondmostTopRatedGames,
            'topRatedGames'                  => $this->topRatedGames,
            'works'                          => $this->works,
        ];
    }
}

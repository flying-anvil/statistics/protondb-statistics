<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Client\ApiClient\DataObject\Nested;

use FlyingAnvil\Libfa\DataObject\DataObject;

class TopCounts implements DataObject
{
    private function __construct(
        private int $countNative,
        private ProtonTopCounts $proton,
    ) {}

    public static function create(int $countNative, ProtonTopCounts $protonTopCounts): self
    {
        return new self($countNative, $protonTopCounts);
    }

    public static function fromRaw(array $rawData): self
    {
        return new self(
            $rawData['native']['count'],
            ProtonTopCounts::fromRaw($rawData['proton']),
        );
    }

    public function getCountNative(): int
    {
        return $this->countNative;
    }

    public function getProton(): ProtonTopCounts
    {
        return $this->proton;
    }

    public function jsonSerialize(): array
    {
        return [
            'countNative' => $this->countNative,
            'proton'      => $this->proton,
        ];
    }
}

<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Client\ApiClient\DataObject\Nested;

use FlyingAnvil\Libfa\DataObject\DataObject;

class Breakdown implements DataObject
{
    private function __construct(
        private TopCounts $topTen,
        private TopCounts $topHundred,
        private TopCounts $topThousand,
    ) {}

    public static function create(TopCounts $topTen, TopCounts $topHundred, TopCounts $topThousand): self
    {
        return new self($topTen, $topHundred, $topThousand);
    }

    public static function fromRaw(array $rawData): self
    {
        return new self(
            TopCounts::fromRaw($rawData['topTen']),
            TopCounts::fromRaw($rawData['topHundred']),
            TopCounts::fromRaw($rawData['topThousand']),
        );
    }

    public function getTopTen(): TopCounts
    {
        return $this->topTen;
    }

    public function getTopHundred(): TopCounts
    {
        return $this->topHundred;
    }

    public function getTopThousand(): TopCounts
    {
        return $this->topThousand;
    }

    public function jsonSerialize(): array
    {
        return [
            'topTen'      => $this->topTen,
            'topHundred'  => $this->topHundred,
            'topThousand' => $this->topThousand,
        ];
    }
}

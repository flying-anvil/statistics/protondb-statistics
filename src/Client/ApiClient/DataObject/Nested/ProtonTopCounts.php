<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Client\ApiClient\DataObject\Nested;

use FlyingAnvil\Libfa\DataObject\DataObject;

class ProtonTopCounts implements DataObject
{
    private function __construct(
        private ProtonTierCounts $rated,
    ) {}

    public static function create(ProtonTierCounts $rated): self
    {
        return new self($rated);
    }

    public static function fromRaw(array $rawData): self
    {
        return new self(
            ProtonTierCounts::fromRaw($rawData['rated']['tierCounts']),
        );
    }

    public function getRated(): ProtonTierCounts
    {
        return $this->rated;
    }

    public function jsonSerialize(): array
    {
        return [
            'rated' => $this->rated,
        ];
    }
}

<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Client\ApiClient\DataObject\Nested;

use FlyingAnvil\Libfa\DataObject\DataObject;

class ProtonTierCounts implements DataObject
{
    private function __construct(
        private int $borked,
        private int $bronze,
        private int $silver,
        private int $gold,
        private int $platinum,
    ) {}

    public static function create(int $borked, int $bronze, int $silver, int $gold, int $platinum): self
    {
        return new self($borked, $bronze, $silver, $gold, $platinum);
    }

    public static function fromRaw(array $rawData): self
    {
        return new self(
            $rawData['borked'] ?? 0,
            $rawData['bronze'] ?? 0,
            $rawData['silver'] ?? 0,
            $rawData['gold'] ?? 0,
            $rawData['platinum'] ?? 0,
        );
    }

    public function getBorked(): int
    {
        return $this->borked;
    }

    public function getBronze(): int
    {
        return $this->bronze;
    }

    public function getSilver(): int
    {
        return $this->silver;
    }

    public function getGold(): int
    {
        return $this->gold;
    }

    public function getPlatinum(): int
    {
        return $this->platinum;
    }

    public function getTotal(): int
    {
        return $this->borked + $this->bronze + $this->silver + $this->gold + $this->platinum;
    }

    public function jsonSerialize(): array
    {
        return [
            'borked'   => $this->borked,
            'bronze'   => $this->bronze,
            'silver'   => $this->silver,
            'gold'     => $this->gold,
            'platinum' => $this->platinum,
        ];
    }
}

<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Client\ApiClient;

use FlyingAnvil\ProtonDbStatistics\Client\ApiClient\DataObject\OverviewResponse;
use FlyingAnvil\ProtonDbStatistics\Client\ApiClient\DataObject\SummaryResponse;
use FlyingAnvil\ProtonDbStatistics\DataObject\Overview;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\RequestOptions;

class ProtonDbApiClient
{
    public const ENDPOINT_SUMMARY_ALL               = 'https://www.protondb.com/data/protondb/by_player_count_with_summaries.json';
    public const ENDPOINT_SUMMARY_SINGLEPLAYER      = 'https://www.protondb.com/data/protondb/by_player_count_with_summaries_singleplayer_no_multiplayer.json';
    public const ENDPOINT_SUMMARY_VR                = 'https://www.protondb.com/data/protondb/by_player_count_with_summaries_vr.json';
    public const ENDPOINT_SUMMARY_INDIE             = 'https://www.protondb.com/data/protondb/by_player_count_with_summaries_indie.json';
    public const ENDPOINT_SUMMARY_LOCAL_MULTIPLAYER = 'https://www.protondb.com/data/protondb/by_player_count_with_summaries_local_multiplayer.json';

    public const ENDPOINT_OVERVIEW = 'https://www.protondb.com/data/counts.json';

    public function __construct(
        private GuzzleClient $guzzleClient,
    ) {}

    public function fetchSummaryAll(): SummaryResponse
    {
        return $this->doFetch(self::ENDPOINT_SUMMARY_ALL);
    }

    public function fetchSummarySingleplayer(): SummaryResponse
    {
        return $this->doFetch(self::ENDPOINT_SUMMARY_SINGLEPLAYER);
    }

    public function fetchSummaryVR(): SummaryResponse
    {
        return $this->doFetch(self::ENDPOINT_SUMMARY_VR);
    }

    public function fetchSummaryIndie(): SummaryResponse
    {
        return $this->doFetch(self::ENDPOINT_SUMMARY_INDIE);
    }

    public function fetchSummaryLocalMultiplayer(): SummaryResponse
    {
        return $this->doFetch(self::ENDPOINT_SUMMARY_LOCAL_MULTIPLAYER);
    }

    public function fetchOverview(): OverviewResponse
    {
        $response = $this->guzzleClient->get(self::ENDPOINT_OVERVIEW, [
            RequestOptions::HTTP_ERRORS => true,
        ]);

        $decoded = json_decode($response->getBody()->getContents(), true, flags: JSON_THROW_ON_ERROR);

        return OverviewResponse::fromRaw($decoded);
    }

    private function doFetch(string $endpoint): SummaryResponse
    {
        $response = $this->guzzleClient->get($endpoint, [
            RequestOptions::HTTP_ERRORS => true,
        ]);

        $decoded = json_decode($response->getBody()->getContents(), true, flags: JSON_THROW_ON_ERROR);

        return SummaryResponse::fromRaw($decoded);
    }
}

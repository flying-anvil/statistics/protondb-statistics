<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Slim;

use FlyingAnvil\Libfa\DataObject\Application\AppEnv;
use Psr\Container\ContainerInterface;
use Slim\App;
use Slim\Factory\AppFactory;

final class SlimAppFactory
{
    public static function create(ContainerInterface $container): App
    {
        $app = AppFactory::createFromContainer($container);

        $middleWareCollector = new RouteMiddlewareCollector();
        $middleWareCollector->register($app, $container->get(AppEnv::class));

        return $app;
    }
}

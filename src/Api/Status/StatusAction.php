<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Api\Status;

use FlyingAnvil\ProtonDbStatistics\Slim\AbstractAction;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;
use Slim\Http\ServerRequest;

class StatusAction extends AbstractAction
{
    public function __invoke(ServerRequest $request, Response $response, array $routeParams = []): ResponseInterface
    {
        return $response->withJson([
            'health' => 'healthy',
        ]);
    }
}

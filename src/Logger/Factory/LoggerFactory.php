<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Logger\Factory;

use FlyingAnvil\Libfa\DataObject\Application\AppEnv;
use FlyingAnvil\ProtonDbStatistics\Options\LoggingOptions;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;

class LoggerFactory
{
    public function __invoke(ContainerInterface $container): Logger
    {
        /**
         * @var AppEnv $appEnv
         * @var LoggingOptions $options
         */
        $appEnv  = $container->get(AppEnv::class);
        $options = $container->get(LoggingOptions::class);

        $stdOutFormatter = new JsonFormatter();
        $stdOutFormatter->setJsonPrettyPrint($appEnv->is(AppEnv::ENV_TESTING));

        $logLevel      = $options->getLogLevel();
        $stdoutHandler = new StreamHandler('php://stdout', $logLevel);
        $fileHandler   = new StreamHandler($options->getLogDirectory() . '/protondb-statistics.log', $logLevel);

        $stdoutHandler->setFormatter($stdOutFormatter);
        $fileHandler->setFormatter(new JsonFormatter());

        $logger = new Logger('ytdl-web');
        $logger->pushHandler($stdoutHandler);
        $logger->pushHandler($fileHandler);

        return $logger;
    }
}

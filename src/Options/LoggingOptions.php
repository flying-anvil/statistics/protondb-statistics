<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Options;

class LoggingOptions
{
    public function __construct(
        private string $logLevel,
        private string $logDirectory,
    ) {}

    public function getLogLevel(): string
    {
        return $this->logLevel;
    }

    public function getLogDirectory(): string
    {
        return $this->logDirectory;
    }
}

<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\DataObject;

use FlyingAnvil\Libfa\DataObject\Time\UtcDate;

class Overview
{
    private function __construct(
        private UtcDate $dateImport,
        private int $timestamp,
        private int $countReports,
        private int $countUniqueGames,
        private int $countUniqueSecondmostNotTopmostRatedGames,
        private int $countUniqueSecondmostTopRatedGames,
        private int $countUniqueTopRatedGames,
        private int $countWorks,
    ) {}

    public static function create(
        UtcDate $dateImport,
        int $timestamp,
        int $countReports,
        int $countUniqueGames,
        int $countUniqueSecondmostNotTopmostRatedGames,
        int $countUniqueSecondmostTopRatedGames,
        int $countUniqueTopRatedGames,
        int $countWorks,
    ): self {
        return new self(
            $dateImport,
            $timestamp,
            $countReports,
            $countUniqueGames,
            $countUniqueSecondmostNotTopmostRatedGames,
            $countUniqueSecondmostTopRatedGames,
            $countUniqueTopRatedGames,
            $countWorks,
        );
    }

    public function getDateImport(): UtcDate
    {
        return $this->dateImport;
    }

    public function getTimestamp(): int
    {
        return $this->timestamp;
    }

    public function getCountReports(): int
    {
        return $this->countReports;
    }

    public function getCountUniqueGames(): int
    {
        return $this->countUniqueGames;
    }

    public function getCountUniqueSecondmostNotTopmostRatedGames(): int
    {
        return $this->countUniqueSecondmostNotTopmostRatedGames;
    }

    public function getCountUniqueSecondmostTopRatedGames(): int
    {
        return $this->countUniqueSecondmostTopRatedGames;
    }

    public function getCountUniqueTopRatedGames(): int
    {
        return $this->countUniqueTopRatedGames;
    }

    public function getCountWorks(): int
    {
        return $this->countWorks;
    }
}

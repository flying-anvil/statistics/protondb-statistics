<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\DataObject;

enum RatingCategory
{
    case All;
    case Singleplayer;
    case VR;
    case Indie;
    case LocalMultiplayer;

    public function toDatabaseString(): string
    {
        return match ($this) {
            self::All => 'all',
            self::Singleplayer => 'single_player',
            self::VR => 'vr',
            self::Indie => 'indie',
            self::LocalMultiplayer => 'local_multiplayer',
        };
    }
}

<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\DataObject;

use FlyingAnvil\Libfa\DataObject\Time\UtcDate;

class Rating
{
    private function __construct(
        private UtcDate        $dateImport,
        private RatingCategory $category,
        private RatingRange    $range,
        private int            $countNative,
        private int            $countBorked,
        private int            $countBronze,
        private int            $countSilber,
        private int            $countGold,
        private int            $countPlatinum,
    ) {}

    public static function create(
        UtcDate        $dateImport,
        RatingCategory $category,
        RatingRange    $range,
        int            $countNative,
        int            $countBorked,
        int            $countBronze,
        int            $countSilber,
        int            $countGold,
        int            $countPlatinum,
    ): self {
        return new self(
            $dateImport,
            $category,
            $range,
            $countNative,
            $countBorked,
            $countBronze,
            $countSilber,
            $countGold,
            $countPlatinum,
        );
    }

    public function getDateImport(): UtcDate
    {
        return $this->dateImport;
    }

    public function getCategory(): RatingCategory
    {
        return $this->category;
    }

    public function getRange(): RatingRange
    {
        return $this->range;
    }

    public function getCountNative(): int
    {
        return $this->countNative;
    }

    public function getCountBorked(): int
    {
        return $this->countBorked;
    }

    public function getCountBronze(): int
    {
        return $this->countBronze;
    }

    public function getCountSilber(): int
    {
        return $this->countSilber;
    }

    public function getCountGold(): int
    {
        return $this->countGold;
    }

    public function getCountPlatinum(): int
    {
        return $this->countPlatinum;
    }
}

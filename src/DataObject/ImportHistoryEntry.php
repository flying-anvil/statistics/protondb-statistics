<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\DataObject;

use FlyingAnvil\Libfa\DataObject\Time\UtcDate;

class ImportHistoryEntry
{
    private function __construct(
        private UtcDate $importDate,
        private ImportType $importType,
    ) {}

    public static function create(UtcDate $importDate, ImportType $importType): self
    {
        return new self($importDate, $importType);
    }

    public function getImportDate(): UtcDate
    {
        return $this->importDate;
    }

    public function getImportType(): ImportType
    {
        return $this->importType;
    }
}

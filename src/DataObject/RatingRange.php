<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\DataObject;

enum RatingRange
{
    case TopTen;
    case TopHundred;
    case TopThousand;

    public function toDatabaseString(): string
    {
        return match ($this) {
            self::TopTen => 'top_ten',
            self::TopHundred => 'top_hundred',
            self::TopThousand => 'top_thousand',
        };
    }
}

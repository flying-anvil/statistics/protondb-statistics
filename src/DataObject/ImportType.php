<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\DataObject;

enum ImportType
{
    case Ratings;
    case Overview;

    public function toDatabaseString(): string
    {
        return match ($this) {
            self::Ratings => 'ratings',
            self::Overview => 'overview',
        };
    }
}

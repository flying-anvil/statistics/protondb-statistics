<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Command;

use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\ProtonDbStatistics\Client\ApiClient\ProtonDbApiClient;
use FlyingAnvil\ProtonDbStatistics\DataObject\ImportHistoryEntry;
use FlyingAnvil\ProtonDbStatistics\DataObject\ImportType;
use FlyingAnvil\ProtonDbStatistics\DataObject\Overview;
use FlyingAnvil\ProtonDbStatistics\Repository\ImportHistoryRepository;
use FlyingAnvil\ProtonDbStatistics\Repository\OverviewRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FetchOverviewCommand extends Command
{
    public const COMMAND_NAME = 'fetch:overview';

    public function __construct(
        private ProtonDbApiClient       $protonDbApiClient,
        private OverviewRepository      $overviewRepository,
        private ImportHistoryRepository $importHistoryRepository,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription('Fetches the current raking data');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $importDate = UtcDate::now();

        $overviewResponse = $this->protonDbApiClient->fetchOverview();
        $overview         = Overview::create(
            $importDate,
            $overviewResponse->getTimestamp(),
            $overviewResponse->getReports(),
            $overviewResponse->getUniqueGames(),
            $overviewResponse->getSecondmostNotTopmostRatedGames(),
            $overviewResponse->getSecondmostTopRatedGames(),
            $overviewResponse->getUniqueGames(),
            $overviewResponse->getWorks(),
        );

        $this->overviewRepository->storeOverview($overview);
        $this->importHistoryRepository->storeImportHistoryEntry(ImportHistoryEntry::create(
            $importDate,
            ImportType::Overview,
        ));

        return self::SUCCESS;
    }
}

<?php

declare(strict_types=1);

namespace FlyingAnvil\ProtonDbStatistics\Command;

use FlyingAnvil\Libfa\DataObject\Time\UtcDate;
use FlyingAnvil\Libfa\Utils\Progress\Options\ProgressBarOptions;
use FlyingAnvil\Libfa\Utils\Progress\ProgressBar;
use FlyingAnvil\ProtonDbStatistics\Client\ApiClient\DataObject\SummaryResponse;
use FlyingAnvil\ProtonDbStatistics\Client\ApiClient\ProtonDbApiClient;
use FlyingAnvil\ProtonDbStatistics\DataObject\ImportHistoryEntry;
use FlyingAnvil\ProtonDbStatistics\DataObject\ImportType;
use FlyingAnvil\ProtonDbStatistics\DataObject\Rating;
use FlyingAnvil\ProtonDbStatistics\DataObject\RatingCategory;
use FlyingAnvil\ProtonDbStatistics\DataObject\RatingRange;
use FlyingAnvil\ProtonDbStatistics\Repository\ImportHistoryRepository;
use FlyingAnvil\ProtonDbStatistics\Repository\RatingRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FetchRatingCommand extends Command
{
    public const COMMAND_NAME = 'fetch:rating';

    public function __construct(
        private ProtonDbApiClient $protonDbApiClient,
        private RatingRepository $ratingRepository,
        private ImportHistoryRepository $importHistoryRepository,
        private ProgressBar $progressBar,
    ) {
        $this->progressBar->setOptions(ProgressBarOptions::createPresetSimpleBlocks());

        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription('Fetches the current raking data');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
//        ini_set('xdebug.var_display_max_depth', '10');
//        ini_set('xdebug.var_display_max_children', '256');
//        ini_set('xdebug.var_display_max_data', '1024');

        $importDate = UtcDate::now();

        $this->progressBar->writeProgress(0, 5);
        $summaryAll              = $this->protonDbApiClient->fetchSummaryAll();
        $this->progressBar->writeProgress(1, 5);
        $summarySingleplayer     = $this->protonDbApiClient->fetchSummarySingleplayer();
        $this->progressBar->writeProgress(2, 5);
        $summaryVR               = $this->protonDbApiClient->fetchSummaryVR();
        $this->progressBar->writeProgress(3, 5);
        $summaryIndie            = $this->protonDbApiClient->fetchSummaryIndie();
        $this->progressBar->writeProgress(4, 5);
        $summaryLocalMultiplayer = $this->protonDbApiClient->fetchSummaryLocalMultiplayer();
        $this->progressBar->writeProgress(5, 5);
        echo PHP_EOL;

        $allRatings = [
            ...$this->responseToRatings($importDate, RatingCategory::All, $summaryAll),
            ...$this->responseToRatings($importDate, RatingCategory::Singleplayer, $summarySingleplayer),
            ...$this->responseToRatings($importDate, RatingCategory::VR, $summaryVR),
            ...$this->responseToRatings($importDate, RatingCategory::Indie, $summaryIndie),
            ...$this->responseToRatings($importDate, RatingCategory::LocalMultiplayer, $summaryLocalMultiplayer),
        ];

        $this->ratingRepository->storeRating(...$allRatings);
        $this->importHistoryRepository->storeImportHistoryEntry(ImportHistoryEntry::create(
            $importDate,
            ImportType::Ratings,
        ));

        return self::SUCCESS;
    }

    private function responseToRatings(
        UtcDate $importDate,
        RatingCategory  $ratingCategory,
        SummaryResponse $summaryResponse,
    ): array {
        $topTen = $summaryResponse->getBreakdown()->getTopTen();
        $topHundred = $summaryResponse->getBreakdown()->getTopHundred();
        $topThousand = $summaryResponse->getBreakdown()->getTopThousand();

        return [
            Rating::create(
                $importDate,
                $ratingCategory,
                RatingRange::TopTen,
                $topTen->getCountNative(),
                $topTen->getProton()->getRated()->getBorked(),
                $topTen->getProton()->getRated()->getBronze(),
                $topTen->getProton()->getRated()->getSilver(),
                $topTen->getProton()->getRated()->getGold(),
                $topTen->getProton()->getRated()->getPlatinum(),
            ),
            Rating::create(
                $importDate,
                $ratingCategory,
                RatingRange::TopHundred,
                $topHundred->getCountNative(),
                $topHundred->getProton()->getRated()->getBorked(),
                $topHundred->getProton()->getRated()->getBronze(),
                $topHundred->getProton()->getRated()->getSilver(),
                $topHundred->getProton()->getRated()->getGold(),
                $topHundred->getProton()->getRated()->getPlatinum(),
            ),
            Rating::create(
                $importDate,
                $ratingCategory,
                RatingRange::TopThousand,
                $topThousand->getCountNative(),
                $topThousand->getProton()->getRated()->getBorked(),
                $topThousand->getProton()->getRated()->getBronze(),
                $topThousand->getProton()->getRated()->getSilver(),
                $topThousand->getProton()->getRated()->getGold(),
                $topThousand->getProton()->getRated()->getPlatinum(),
            ),
        ];
    }
}

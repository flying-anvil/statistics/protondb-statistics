CREATE TABLE `overview`
(
    `importCount`                                     int unsigned NOT NULL AUTO_INCREMENT,
    `date_import`                                     datetime     NOT NULL,
    `timestamp`                                       int          NOT NULL,
    `count_reports`                                   int unsigned,
    `count_unique_games`                              int unsigned,
    `count_unique_secondmost_not_topmost_rated_games` int unsigned,
    `count_unique_secondmost_top_rated_games`         int unsigned,
    `count_unique_top_rated_games`                    int unsigned,
    `count_works`                                     int unsigned,
    PRIMARY KEY (`importCount`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE `import_history`
(
    `import_count` int unsigned                 NOT NULL AUTO_INCREMENT,
    `date_import`  datetime                     NOT NULL,
    `import_type`  enum ('ratings', 'overview') NOT NULL,
#     `timestamp`   timestamp    NOT NULL,
    PRIMARY KEY (`import_count`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

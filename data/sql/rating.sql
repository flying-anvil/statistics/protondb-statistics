CREATE TABLE `ratings`
(
    `date_import`    datetime                                                          NOT NULL,
    `category`       enum ('all', 'single_player', 'vr', 'indie', 'local_multiplayer') NOT NULL,
    `range`          enum ('top_ten', 'top_hundred', 'top_thousand')                   NOT NULL,
    `count_native`   int unsigned                                                      NOT NULL,
    `count_borked`   int unsigned                                                      NOT NULL,
    `count_bronze`   int unsigned                                                      NOT NULL,
    `count_silber`   int unsigned                                                      NOT NULL,
    `count_gold`     int unsigned                                                      NOT NULL,
    `count_platinum` int unsigned                                                      NOT NULL,
    PRIMARY KEY (`date_import`, `category`, `range`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
